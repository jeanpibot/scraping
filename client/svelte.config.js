module.exports = {
  publicPath: process.env.NODE_ENV === "production"
    ? "/production-sub-path/"
    : "/",
  pages: {
    index: {
      // entry for the page
      entry: "src/index.js",
      // the source template
      template: "public/index.html",
      // output as dist/index.html
      filename: "index.html",
      // when using title option,
      // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
      title: "Index Page",
      // chunks to include on this page, by default includes
      // extracted common chunks and vendor chunks.
      chunks: ["chunk-vendors", "chunk-common", "index"],
    },
  },
  devServer: {
    proxy: {
      "/api/*": {
        target: "http://localhost:8000",
      },
    },
  },
};
