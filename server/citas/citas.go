package citas

type Getter interface {
	GetAll() []Quote
}

type Adder interface {
	Add(cita Quote)
}

type Quote struct {
	Quote  string `json:"quote"`
	Author string `json:"author"`
}

type Repo struct {
	Quotes []Quote
}

func New() *Repo {
	return &Repo{
		Quotes: []Quote{},
	}
}

func (r *Repo) Add(cita Quote) {
	r.Quotes = append(r.Quotes, cita)
}

func (r *Repo) GetAll() []Quote {
	return r.Quotes
}
